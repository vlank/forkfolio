import { useState } from "react";
import Navbar from "./components/Navbar";
import theme from "./styles/palette";
import "./App.css";
import "./styles/presentation.css";
import "./styles/skills.css";
import "./styles/about.css";
import "./styles/contact.css";
import "./styles/project.css";
import { ThemeProvider, Typography } from "@mui/material";
import Presentation from "./components/Presentation";
import Skills from "./components/Skills";
import About from "./components/About";
import Contact from "./components/Contact";
import Projects from "./components/Projects";

function App() {
  const [count, setCount] = useState(0);

  return (
    <>
      <ThemeProvider theme={theme}>
        <Navbar></Navbar>
        <section className="main-presentation">
          <div className="main-background"></div>
          <div className="ghost-container" >
            <img className="ghost-mobile" src="/ghost.svg"></img>
          </div>
          <Presentation></Presentation>
        </section>
        <section id="stack" className="main-skills">
          <Skills></Skills>
        </section>
        <section id="about" className="main-about">
        <div className="main-background2"></div>
          <About></About>
        </section>

        <section id="proyectos" className="main-projects">
          <Projects></Projects>
        </section>

        <section id="contact" className="main-contact">
        <div className="main-background3"></div>
          <Contact></Contact>
        </section>
        <footer>
          <Typography variant="body" my={4}>Hecho en 24hs sin saber nada de React</Typography>
          <Typography variant="body2" my={2}>Hace la diferencia, esto ya lo vimos todos.</Typography>
          <Typography variant="button">
            Copyright 2023 © Vlank
          </Typography>
        </footer>
      </ThemeProvider>

     
    </>
  );
}

export default App;

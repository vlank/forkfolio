import { Typography } from "@mui/material";

function About() {
  return (
    <div className="about-back">
      <Typography variant="h4" sx={{ color: "secondary.main", my: 5 }}>
        Sobre mí
      </Typography>
      <Typography variant="body1" className="skills-text">
      Mi historia comienza con un proyecto que me llevó directamente a la cima. No fue un proyecto cualquiera, fue <b>EL</b> proyecto. Con solo una línea de código y un sublime README.md, conquisté el mundo de la programación. Las tecnologías no eran un problema para mí, dominé todo, desde React hasta Angular, con un solo git clone. Eso es lo que llamo eficiencia.
      </Typography>
      <Typography variant="body1" className="skills-text">
      El manejo de Git es un arte que pocos pueden comprender. En lugar de desperdiciar mi tiempo con cientos de commits inútiles, he perfeccionado la técnica del "commit único". Menos es más, ¿verdad? Así que, si alguna vez te encuentras preguntándote cómo mantener un repositorio limpio y ordenado, solo tienes que preguntarme a mí. Te revelaré el secreto detrás de mis misteriosos menos de 10 commits.
      </Typography>
      <Typography variant="body1" className="skills-text">
      Mi historia es una montaña rusa de eventos irrelevantes que, sorprendentemente, me llevaron a convertirme en el desarrollador de un solo proyecto más influyente del mundo. Crecí, respiré y soñé con líneas de código, y aquí estoy, listo para conquistar el mundo digital.
      </Typography>
      <Typography variant="overline">En colaboración con CHATGPT</Typography>

    </div>
  );
}

export default About;

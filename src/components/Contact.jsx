import { Button, TextField, Typography } from "@mui/material";

function Contact() {
  return (
    <div className="contact-main">
      <Typography variant="h4" sx={{ color: "secondary.main", my: 10 }}>
        Contacto
      </Typography>
      <div className="contact-form">
        <TextField
          title="¿En serio? ¿exponer un correo en 2023?"
          label="Correo"
          color="secondary"
          focused
          InputProps={{ inputProps: { style: { color: '#fff'}}}}
        />
        <Button color="secondary" variant="contained" sx={{ my: 2, width: "10%",}}>
          Enviar
        </Button>
      </div>
    </div>
  );
}

export default Contact;

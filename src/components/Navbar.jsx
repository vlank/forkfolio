import NavbarItem from "./NavbarItem";
import { AppBar, List } from "@mui/material";
import forky from "/forky.svg";
import useMediaQuery from "@mui/material/useMediaQuery";

function SimpleMediaQuery() {
  const matches = useMediaQuery("(max-width:450px)");
  if (matches) {
    return 24;
  } else {
    return 35;
  }
}
function SimpleMediaQuery2() {
  const matches = useMediaQuery("(max-width:450px)");
  if (matches) {
    return 20;
  } else {
    return 40;
  }
}

function Navbar() {
  return (
    <AppBar
      sx={{
        display: "flex",
        width: "100%",
        height: "10svh",
        position: "sticky",
        background: "none",
        boxShadow: "none",
      }}
    >
      <div
        style={{
          display: "flex",
          marginLeft:  SimpleMediaQuery2(),
          marginRight:  SimpleMediaQuery2(),
          height: "100%",
          alignItems: "center",
        }}
      >
        <img
          src={forky}
          className="forky"
          alt="Forky logo"
          width={SimpleMediaQuery()}
          height={SimpleMediaQuery()}
        />
        <List
          sx={{
            display: "flex",
            width: 1 / 4,
            my: 1,
            mx: 2,
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <NavbarItem text="Stack" link="#stack" />
          <NavbarItem text="Sobre" link="#about" />
          <NavbarItem text="Proyectos" link="#proyectos" />
          <NavbarItem text="Contacto" link="#contact" />
        </List>
      </div>
    </AppBar>
  );
}

export default Navbar;

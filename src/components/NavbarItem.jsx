import { Box, Button } from "@mui/material";
import useMediaQuery from '@mui/material/useMediaQuery';

function SimpleMediaQuery() {
  const matches = useMediaQuery('(max-width:450px)');
  if(matches){
    return 8;
  } else {
    return 12;
  }
}

function NavbarItem(props){
  return ( <Box>
  <Button href={props.link} variant="button" sx={{borderRadius:25, color:"secondary.light", fontWeight:300, fontSize:SimpleMediaQuery()}}>{props.text}</Button>
  </Box>
  );
}

NavbarItem.defaultProps = {
  text: 'Navbar item',
  link: '#'
}

export default NavbarItem;
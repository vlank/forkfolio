import { Grid, Typography } from "@mui/material";
import { Typewriter } from "react-simple-typewriter";

function calculateExperienceDays(targetDate) {
  //ChatGPT function
  const currentDate = new Date();
  const timeDifference = currentDate -targetDate ;
  const daysDifference = Math.floor(timeDifference / (1000 * 60 * 60 * 24));
  return daysDifference;
}

function Presentation() {
  const targetDate = new Date("2023-09-22");
  const xp = calculateExperienceDays(targetDate);
  const dia = (xp === 1) ? "día" : "días";
  
  return (
    <Grid container className="presentation">
      <Grid item xs={6}>
        <div className="presentation-title">
          <Typography variant="h2" sx={{ color: "secondary.main" }}>
            GhostDEV
          </Typography>
          <Typography variant="h5" sx={{ fontWeight:200, width:"80%"}}>
            Desarrollador frontend desde hace {xp} {dia}
          </Typography>
          <Typography variant="h6" sx={{fontWeight:200}}>
            me destaco por {' '}
            <Typewriter
              words={["sobrecalificarme", "clonar proyectos","el minimalismo de mis commits","scrolles infinitos"]}
              loop={0}
              typeSpeed={70}
              deleteSpeed={50}
              delaySpeed={1000}
            ></Typewriter>
          </Typography>
        </div>
      </Grid>

      <Grid item xs={6} className="ghost-image">
        <img src="/ghost.svg"></img>
      </Grid>
    </Grid>
  );
}

export default Presentation;

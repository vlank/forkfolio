import { Typography } from "@mui/material";
import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';
const handleDragStart = (e) => e.preventDefault();

const items = [
  <img className="project-image" src="./projects/p1.png" onDragStart={handleDragStart} role="presentation" />,
  <img className="project-image" src="./projects/p2.png" onDragStart={handleDragStart} role="presentation" />,
  <img className="project-image" src="./projects/p3.jpg" onDragStart={handleDragStart} role="presentation" />,
  <img className="project-image" src="./projects/p4.jpg" onDragStart={handleDragStart} role="presentation" />,
  <img className="project-image" src="./projects/p5.jpg" onDragStart={handleDragStart} role="presentation" />,
  <img className="project-image" src="./projects/p6.jpg" onDragStart={handleDragStart} role="presentation" />,
];


function Projects() {
  return (
    <div className="about-back">
      <Typography variant="h4" sx={{ color: "secondary.main", my: 5 }}>
        Proyectos
      </Typography>
      <Typography variant="body1" className="skills-text">
      Estos proyectos, que son verdaderamente únicos en su enfoque, reflejan mi habilidad para inspirarme en las mejores prácticas de la comunidad.
      Me destaco por aplicar un enfoque <b>innovador</b> para resolver problemas en corto tiempo. Consiste en aprovechar al máximo las soluciones probadas por otros en lugar de reinventar la rueda.
      En cuanto a mis habilidades tecnológicas, puedo decir con confianza que las conozco a fondo. 
      En resumen, mi portafolio está lleno de proyectos que revelan mi aprecio por el conocimiento compartido y la comunidad de desarrolladores. 
      </Typography>
      <Typography variant="caption" sx={{ color: "secondary.main", my: 5 }}>
        No paso el código porque me lo roban
      </Typography>
        <AliceCarousel disableButtonsControls="true" mouseTracking items={items} />
    </div>
  );
}

export default Projects;

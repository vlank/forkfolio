import { Typography } from "@mui/material";
import {
  Github_Light,
  JavaScript,
  React_Light,
  Astro,
  Ableton_Light,
  AfterEffects,
  AlpineJS_Light,
  Instagram,
  Kotlin_Light,
  MaterialUI_Light,
  Netlify_Light,
  LinkedIn,
  Java_Light,
  Angular_Light,
  Arduino,
  Audition,
  AutoCAD_Light,
  Azure_Light,
  Bash_Light,
  Bootstrap,
  C,
  CSS,
  MySQL_Light,
  CodePen_Light,
  Dart_Light,
  Discord,
  Laravel_Light,
  Django,
  Docker,
  ExpressJS_Light,
  Fediverse_Light,
  Figma_Light,
  NestJS_Light,
  Firebase_Light,
  Flutter_Light,
  GitLab_Light,
  GoLang,
  Heroku,
  HTML,
  Markdown_Light,
} from "react-skillicons";

function Skills() {
  const percentage = 40;
  return (
    <>
      <Typography variant="h4" sx={{ color: "secondary.main", my: 5 }}>
        Stack
      </Typography>
      <Typography variant="subtitle1" sx={{ color: "white", my: 2 }}>
        ¿Más es mejor no?
      </Typography>
      <div className="skills-img">
        <Github_Light className="iconSkills" />
        <JavaScript className="iconSkills" />
        <React_Light className="iconSkills" />
        <Astro className="iconSkills" />
        <Ableton_Light className="iconSkills" />
        <AfterEffects className="iconSkills" />
        <AlpineJS_Light className="iconSkills" />
        <Angular_Light className="iconSkills" />
        <Arduino className="iconSkills" />
        <Audition className="iconSkills" />
      </div>
      <div className="skills-img">
        <AutoCAD_Light className="iconSkills" />
        <Azure_Light className="iconSkills" />
        <Bash_Light className="iconSkills" />
        <Bootstrap className="iconSkills" />
        <C className="iconSkills" />
        <CSS className="iconSkills" />
        <CodePen_Light className="iconSkills" />
        <Dart_Light className="iconSkills" />
        <Discord className="iconSkills" />
        <Django className="iconSkills" />
      </div>
      <div className="skills-img">
        <Docker className="iconSkills" />
        <ExpressJS_Light className="iconSkills" />
        <Fediverse_Light className="iconSkills" />
        <Figma_Light className="iconSkills" />
        <Firebase_Light className="iconSkills" />
        <Flutter_Light className="iconSkills" />
        <GitLab_Light className="iconSkills" />
        <GoLang className="iconSkills" />
        <HTML className="iconSkills" />
        <Heroku className="iconSkills" />
      </div>
      <div className="skills-img">
        <Instagram className="iconSkills" />
        <Java_Light className="iconSkills" />
        <Kotlin_Light className="iconSkills" />
        <Laravel_Light className="iconSkills" />
        <LinkedIn className="iconSkills" />
        <MaterialUI_Light className="iconSkills" />
        <MySQL_Light className="iconSkills" />
        <Netlify_Light className="iconSkills" />
        <NestJS_Light className="iconSkills" />
        <Markdown_Light className="iconSkills" />
      </div>
    </>
  );
}

export default Skills;

import { createTheme } from '@mui/material/styles'

const theme = createTheme({
  typography: {
    fontFamily: [
      'Lexend',
      'Inter'
    ].join(','),
  },
  palette: {
    raw: {
      main:'#4d4d4d'
    },
    primary: {
      main: '#5B47F9',
      light: '#8879F6',
      contrastText: '#ffffff',
      dark: '#aaaaaa',
    },
    secondary: {
      main: '#13F9C7',
    },
    error: {
      main: '#FA7272',
    },
    warning: {
      main: '#E9B44C',
    },
    info: {
      main: '#62C0C0',
    },
    success: {
      main: '#61DD87',
    },
  },
})

export default theme
